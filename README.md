# Trimir

A life journal for you and your tribe peers — No Internet Required.

## Development Environment

- Node.js, LTS (as of writing `v10.15.2`)
- [Create React App](https://create-react-app.dev/)

## Target Environment(s)

1. **Web** —› PWA (Progressive Web App)
2. Mobile —› Native (Android, iOS)

## License

[AGPLv3 or later](./LICENSE)